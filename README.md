# README #

### What is this repository for? ###


This repository is a open source demo app for the emergencycorridor application on android.

### How do I get set up? ###

To use this library this app you have also to download the [emergencycorridor library for android](https://bitbucket.org/ipimlandshut/emergencycorridorandroidlibrary).
Download it into and store it paralel to the this app. The dependency is already configured in the gradle file.
To recieve live emergency notifications you have to contact the developers and send the package name of your project.
Hereafter, we can send you an google-service.json which has to be integrated in this app.
When this requierments are met you will recieve the emeregencycorridor messages.


### Who do I talk to? ###

[contact us](mailto:julian.doerndorfer@haw-landshut.de)