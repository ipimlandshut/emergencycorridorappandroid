package de.hawla.ipim.emergencycorridor;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import de.hawla.ipim.emergencycorridorlibrary.activity.MainActivity;
import de.hawla.ipim.emergencycorridorlibrary.model.Constants;
import de.hawla.ipim.emergencycorridorlibrary.service.EmergencyCorridorMessageService;

public class StartActivity extends AppCompatActivity {

    private static final String TAG = "StartActivity";
    private TextToSpeech textToSpeech;

    public void emergencyCorridorOnClick(View view) {
    }

    private boolean getEmergencyCorridorMessages;
    private EmergencyCorridorMessageService emergencyCorridorMessageService;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        emergencyCorridorMessageService = new EmergencyCorridorMessageService();
        Switch emergencyCorridorSwitch = (Switch) findViewById(R.id.emergencyMessageSwitch);
        getEmergencyCorridorMessages = emergencyCorridorSwitch.isChecked();
        setEmergencyCorridorSwitchListener(emergencyCorridorSwitch);
        handleEmergencyCorridorMessages();

    }

    private void setEmergencyCorridorSwitchListener(Switch emergencyCorridorSwitch) {
        emergencyCorridorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getEmergencyCorridorMessages = isChecked;
                handleEmergencyCorridorMessages();
            }
        });
    }

    private void handleEmergencyCorridorMessages(){
        if(getEmergencyCorridorMessages){
            emergencyCorridorMessageService.receiveMessages();
        } else {
            emergencyCorridorMessageService.stopReceivingMessages(this.getApplicationContext());
        }
    }




    @Override
    public void onStart(){
        if(!checkLocationPermission()){
            requestLocationPermissions();
        } else if(!checkLogPermissions()){
            requestLogPermissions();
        }
        super.onStart();

    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkLocationPermission() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }
    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkLogPermissions() {
        int permissionStateWriteExternalStorage = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionStateReadExternalStorage = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        return permissionStateReadExternalStorage == PackageManager.PERMISSION_GRANTED && permissionStateWriteExternalStorage == PackageManager.PERMISSION_GRANTED;
    }

    private void requestLocationPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(StartActivity.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                    Constants.REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(StartActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void requestLogPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) &&
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_log, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(StartActivity.this,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    Constants.REQUEST_PERMISSIONS_REQUEST_CODE);
                            ActivityCompat.requestPermissions(StartActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    Constants.REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(StartActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    Constants.REQUEST_PERMISSIONS_REQUEST_CODE);
            ActivityCompat.requestPermissions(StartActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    private void createLogFile(){
        Log.d(TAG, "Log file will be created.");
        if ( isExternalStorageWritable() ) {

            File appDirectory = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+ "/Rettungsgasse" );
            File logDirectory = new File( appDirectory + "/log" );
            SimpleDateFormat sdf = new SimpleDateFormat("yyyymmdd_HHmmss", Locale.getDefault());
            Date resultdate = new Date(System.currentTimeMillis());
            File logFile = new File( logDirectory, "logcat"+ sdf.format(resultdate) +".txt" );
            Log.d(TAG, "Log file will be stored in path: " + logFile.getAbsolutePath());
            Toast.makeText(this, "Log file will be stored in path: " + logFile.getAbsolutePath(), Toast.LENGTH_LONG).show();

            // create app folder
            if ( !appDirectory.exists() ) {
                appDirectory.mkdir();
            }

            // create log folder
            if ( !logDirectory.exists() ) {
                logDirectory.mkdir();
            }

            // clear the previous logcat and then write the new one to the file
            try {
//                Process process = Runtime.getRuntime().exec( "logcat -c");
                Process process = Runtime.getRuntime().exec( "logcat -f " + logFile.getAbsolutePath() );

                Log.d(TAG, "log stored");
            } catch ( IOException e ) {
                Log.e(TAG, "IOEception thrwon", e);

            }

        } else if ( isExternalStorageReadable() ) {
            Log.e(TAG, "external storage is only readable.");
        } else {
            Log.e(TAG, "external storage is not accessible.");
            // not accessible
        }
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals( state ) ) {
            return true;
        }
        return false;
    }

    public void logSaveButtononClick(View view) {
        createLogFile();
    }

    public void demoButtonClick(View view) {
        showEmergencyCorridorActivity();
        announceEmergencyCorridorMessage();
    }

    private void showEmergencyCorridorActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }
    private void announceEmergencyCorridorMessage() {
        final String speechString = getResources().getString(haw.landshut.de.emergencycorridorlibrary.R.string.emergency_corridor_text);
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    textToSpeech.setLanguage(Locale.GERMAN);
                    textToSpeech.speak(speechString, TextToSpeech.QUEUE_FLUSH, null, null);
                } else {
                    Log.e(TAG, "TextToSpeech not successful");
                }
            }
        });
    }



}
